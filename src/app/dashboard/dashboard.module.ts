import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Route } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { VideoDashboardComponent } from './video-dashboard/video-dashboard.component';
import { VideoListComponent } from './video-list/video-list.component';
import { VideoPlayerComponent } from './video-player/video-player.component';
import { StatFiltersComponent } from './stat-filters/stat-filters.component';
import { VideoListItemComponent } from './video-list-item/video-list-item.component';
import { DashboardState, selectedVideoReducer, videosReducer } from './state';
import { DashboardEffects } from './effects';

const routes: Route[] = [
  { path: '', component: VideoDashboardComponent, pathMatch: 'full' },
];

@NgModule({
  declarations: [VideoDashboardComponent, VideoListComponent, VideoPlayerComponent, StatFiltersComponent, VideoListItemComponent],
  imports: [
    CommonModule, // Needed for *ngIf & *ngFor
    RouterModule.forChild(routes),
    HttpClientModule,
    ReactiveFormsModule,
    StoreModule.forRoot<DashboardState>({
      selectedVideo: selectedVideoReducer,
      videos: videosReducer,
    }),
    EffectsModule.forRoot([DashboardEffects]),
  ]
})
export class DashboardModule { }
