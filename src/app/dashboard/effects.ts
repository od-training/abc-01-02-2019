import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, Effect, ROOT_EFFECTS_INIT, ofType } from '@ngrx/effects';
import { switchMap, map } from 'rxjs/operators';

import { VideosReceivedAction } from './state';
import { Video } from './types';

@Injectable()
export class DashboardEffects {

  @Effect()
  loadData = this.actions.pipe(
    ofType(ROOT_EFFECTS_INIT),
    switchMap(() => this.http.get<Video[]>('http://api.angularbootcamp.com/videos')),
    map(videos => new VideosReceivedAction(videos)),
  );

  constructor(private actions: Actions, private http: HttpClient) { }

}
