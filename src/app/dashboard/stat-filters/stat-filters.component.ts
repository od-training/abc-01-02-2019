import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css']
})
export class StatFiltersComponent implements OnInit {

  filters: FormGroup;

  constructor(fb: FormBuilder) {
    this.filters = fb.group({
      region: ['all'],
      from: ['2000-01-01'],
      to: ['2020-01-01'],
      under18: [true],
      '18to40': [true],
      '40to60': [true],
      'over60': [true],
    });
  }

  ngOnInit() {
  }

}
