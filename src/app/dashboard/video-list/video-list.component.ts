import { SelectVideoAction } from './../state';
import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Video } from '../types';
import { DashboardState } from '../state';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {

  selectedVideo: Observable<Video>;
  videos: Observable<Video[]>;

  constructor(private store: Store<DashboardState>) {
    this.selectedVideo = store.pipe(select(state => state.selectedVideo));
    this.videos = store.pipe(select(state => state.videos));
  }

  ngOnInit() {
  }

  selectVideo(video: Video) {
    this.store.dispatch(new SelectVideoAction(video));
  }

}
