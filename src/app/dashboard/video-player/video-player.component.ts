import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { map, filter } from 'rxjs/operators';

import { DashboardState } from '../state';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent implements OnInit {

  sanitizedVideoUrl: Observable<SafeResourceUrl>;

  constructor(store: Store<DashboardState>, sanitizer: DomSanitizer) {
    this.sanitizedVideoUrl = store.pipe(
      select(state => state.selectedVideo),
      filter(video => !!video),
      map(video => sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + video.id)),
    );
  }

  ngOnInit() {
  }

}
