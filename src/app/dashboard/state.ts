import { Action } from '@ngrx/store';

import { Video } from './types';

export interface DashboardState {
  videos: Video[];
  selectedVideo: Video;
}

const SELECT_VIDEO = 'SELECT_VIDEO';
export class SelectVideoAction implements Action {
  type = SELECT_VIDEO;
  constructor(public video: Video) { }
}

const VIDEOS_RECEIVED = 'VIDEOS_RECEIVED';
export class VideosReceivedAction implements Action {
  type = VIDEOS_RECEIVED;
  constructor(public videos: Video[]) { }
}

export function selectedVideoReducer(prev: Video, action: Action): Video {
  switch (action.type) {
    case SELECT_VIDEO:
      return (action as SelectVideoAction).video;
    default:
      return prev;
  }
}

export function videosReducer(prev: Video[] = [], action: Action): Video[] {
  switch (action.type) {
    case VIDEOS_RECEIVED:
      return (action as VideosReceivedAction).videos;
    default:
      return prev;
  }
}
